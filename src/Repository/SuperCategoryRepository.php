<?php

namespace App\Repository;

use App\Entity\SuperCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SuperCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method SuperCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method SuperCategory[]    findAll()
 * @method SuperCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SuperCategoryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SuperCategory::class);
    }

    // /**
    //  * @return SuperCategory[] Returns an array of SuperCategory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SuperCategory
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
