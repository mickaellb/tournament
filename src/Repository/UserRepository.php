<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\UserFilter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function loadUserByUsername($username)
    {
        return $this->createQueryBuilder('u')
            ->where('u.username = :username')
            ->setParameter('username', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findAllQuery(UserFilter $filter): Query
    {
        $query = $this->findAllByDate();

        if ($filter->getRole()) {
            if ($filter->getRole() === 1) {
                $query = $query
                    ->andWhere('u.roles LIKE :role1')
                    ->andWhere('u.roles NOT LIKE :role2')
                    ->setParameter('role1', '%USER%')
                    ->setParameter('role2', '%ADMIN%');
            } else if ($filter->getRole() === 2){
                $query = $query
                    ->andWhere('u.roles LIKE :role')
                    ->setParameter('role', "%ROLE_ADMIN%");
            } else {
                throw new \OutOfRangeException("Invalid role" . dump($filter->getRole()));
            }
        }

        if ($filter->getIsActive() !== null) {
            $query = $query
                ->andWhere('u.isActive = :isActive')
                ->setParameter('isActive', $filter->getIsActive());
        }

        return $query->getQuery();
    }

    public function findAllByDate()
    {
        return $this->createQueryBuilder('u')
            ->orderBy('u.createdAt', 'DESC');
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
