<?php
namespace App\Entity;

class UserFilter{
    /**
     * @var string|null
     */
    private $role;

    /**
     * @var boolean|null
     */
    private $isActive;

    /**
     * @return int|null
     */
    public function getRole(): ?string
    {
        return $this->role;
    }

    /**
     * @param int|null $role
     * @return UserFilter
     */
    public function setRole(string $role): UserFilter
    {
        $this->role = $role;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    /**
     * @param bool|null $isActive
     * @return UserFilter
     */
    public function setIsActive(bool $isActive): UserFilter
    {
        $this->isActive = $isActive;
        return $this;
    }


}