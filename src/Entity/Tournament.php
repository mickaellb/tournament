<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TournamentRepository")
 * @UniqueEntity(
 *     fields={"title"},
 *     message="Ce titre n'est pas disponible."
 * )
 */
class Tournament
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=5, max=50)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $team;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min=2, max=100)
     */
    private $size;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Range(min=1, max=10)
     */
    private $teamMinSize;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Range(max=10)
     */
    private $teamMaxSize;

    /**
     * @ORM\Column(type="boolean")
     */
    private $public;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="Tournaments")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="tournaments")
     * @ORM\JoinColumn(nullable=false)
     */
    private $User;

    /**
     * @ORM\Column(type="boolean")
     */
    private $state;

    /**
     * @Assert\Callback
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if($this->getTeamMaxSize()<$this->getTeamMinSize()){
            $context->buildViolation("La valeur de ce champ doit être supérieur à la taille de l'équipe")
            ->atPath('teamMaxSize')
            ->addViolation();
        }

        if($this->getTeam() == 0 && $this->getTeamMinSize() !== 1){
            $context->buildViolation("La taille de l'équipe doit être de 1 pour un tournoi en Solo")
                ->atPath('teamMinSize')
                ->addViolation();
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getTeam(): ?bool
    {
        return $this->team;
    }

    public function setTeam(bool $team): self
    {
        $this->team = $team;

        return $this;
    }

    public function getSize(): ?int
    {
        return $this->size;
    }

    public function setSize(int $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getTeamMinSize(): ?int
    {
        return $this->teamMinSize;
    }

    public function setTeamMinSize(int $teamMinSize): self
    {
        $this->teamMinSize = $teamMinSize;

        return $this;
    }

    public function getTeamMaxSize(): ?int
    {
        return $this->teamMaxSize;
    }

    public function setTeamMaxSize(int $teamMaxSize): self
    {
        $this->teamMaxSize = $teamMaxSize;

        return $this;
    }

    public function getPublic(): ?bool
    {
        return $this->public;
    }

    public function setPublic(bool $public): self
    {
        $this->public = $public;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }

    public function getState(): ?bool
    {
        return $this->state;
    }

    public function setState(bool $state): self
    {
        $this->state = $state;

        return $this;
    }
}
