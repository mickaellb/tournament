<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Image;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tournament", mappedBy="category")
     */
    private $Tournaments;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SuperCategory", inversedBy="Categories")
     * @ORM\JoinColumn(nullable=false)
     */
    private $SuperCategory;

    public function __construct()
    {
        $this->Tournaments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->Title;
    }

    public function setTitle(string $Title): self
    {
        $this->Title = $Title;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->Image;
    }

    public function setImage(string $Image): self
    {
        $this->Image = $Image;

        return $this;
    }

    /**
     * @return Collection|Tournament[]
     */
    public function getTournaments(): Collection
    {
        return $this->Tournaments;
    }

    public function addTournament(Tournament $tournament): self
    {
        if (!$this->Tournaments->contains($tournament)) {
            $this->Tournaments[] = $tournament;
            $tournament->setCategory($this);
        }

        return $this;
    }

    public function removeTournament(Tournament $tournament): self
    {
        if ($this->Tournaments->contains($tournament)) {
            $this->Tournaments->removeElement($tournament);
            // set the owning side to null (unless already changed)
            if ($tournament->getCategory() === $this) {
                $tournament->setCategory(null);
            }
        }

        return $this;
    }

    public function getSuperCategory(): ?SuperCategory
    {
        return $this->SuperCategory;
    }

    public function setSuperCategory(SuperCategory $SuperCategory): self
    {
        $this->SuperCategory = $SuperCategory;

        return $this;
    }
}
