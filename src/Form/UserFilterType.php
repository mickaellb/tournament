<?php

namespace App\Form;

use App\Entity\UserFilter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('role', ChoiceType::class, [
                'label' => false,
                'choices' => $this->getRoleChoices()
            ])
            ->add('isActive', ChoiceType::class, [
                'label' => false,
                'choices' => $this->getActiveChoices()
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserFilter::class,
            'method' => 'get',
            'csrf_protection' => false
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }

    private function getActiveChoices()
    {
        $output = [];
        $output['Activés'] = 1;
        $output['Désactivés'] = 0;
        return $output;
    }

    private function getRoleChoices(){
        $output = [];
        $output['Tous'] = 0;
        $output['Utilisateurs'] = 1;
        $output['Admins'] = 2;
        return $output;
    }
}
