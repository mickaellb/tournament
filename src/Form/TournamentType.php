<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\SuperCategory;
use App\Entity\Tournament;
use App\Repository\CategoryRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\Query\Expr\Join;

class TournamentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        switch($options['flow_step'])
        {
            case 1:

                $builder->add('Category', EntityType::class, [
                    'class' => Category::class,
                    'query_builder' => function(CategoryRepository $categoryRepository) use ($options) {
                      return $categoryRepository->findAllBySuperCategoryCode($options['code']);
                    },
                    'choice_label' => 'Title'
                ]);
                break;
            case 2:
                $builder->add('title')
                    ->add('description', TextareaType::class, [
                        'attr' => array('rows' => 5)
                    ]);
                break;


        }

//        $builder
//            ->add('title')
//            ->add('description', TextareaType::class, [
//                'attr' => array('rows' => 5)
//            ])
//            ->add('category', EntityType::class, [
//                'class' => Category::class,
//                'choice_label' => 'Title'
//            ])
//            ->add('team', ChoiceType::class, [
//                'choices' => $this->getTeamChoices()
//            ])
//            ->add('public', ChoiceType::class, [
//                'choices' => $this->getPublicChoices()
//            ])
//            ->add('size')
//            ->add('teamMinSize', NumberType::class, array(
//                'required' => false,
//                'empty_data' => 1
//            ))
//            ->add('teamMaxSize', NumberType::class, array(
//                'required' => false,
//                'empty_data' => 1
//            ))
//        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Tournament::class,
            'translation_domain' => 'forms',
            'code' => null,
        ]);
    }

    private function getTeamChoices()
    {
        $output = [];
        $output['Solo'] = 0;
        $output['Équipe'] = 1;
        return $output;
    }

    private function getPublicChoices()
    {
        $output = [];
        $output['Tournoi Public'] = 1;
        $output['Tournoi Privé'] = 0;
        return $output;
    }
}
