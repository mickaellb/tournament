<?php

namespace App\Form;

use Craue\FormFlowBundle\Form\FormFlow;
use Craue\FormFlowBundle\Form\FormFlowInterface;
use App\Form\TournamentType;

class TournamentFlow extends FormFlow
{
    /**
     * @return array
     */
    protected function loadStepsConfig()
    {
        return [
            1 =>[
                'label' => 'Category',
                'form_type' => TournamentType::class,
            ],
            2 =>[
                'label' => 'Informations',
                'form_type' => TournamentType::class,
            ],
            3 =>[
                'label' => 'Paramètres',
                'form_type' => TournamentType::class
            ],
        ];
    }

}