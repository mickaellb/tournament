<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Tournament;
use App\Form\TournamentFlow;
use App\Form\TournamentType;
use App\Repository\CategoryRepository;
use App\Repository\SuperCategoryRepository;
use App\Repository\TournamentRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TournamentController extends AbstractController
{

    /**
     * @var ObjectManager
     */
    private $em;
    /**
     * @var TournamentFlow
     */
    private $tournamentFlow;

    public function __construct(ObjectManager $em, TournamentFlow $tournamentFlow)
    {
        $this->em = $em;
        $this->tournamentFlow = $tournamentFlow;
    }

    /**
     * @Route("/", name="home")
    */
    public function index(){
        return $this->redirectToRoute('tournaments');
    }

    /**
     * @Route("/tournaments", name="tournaments")
     * @param TournamentRepository $repo
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function tournaments(TournamentRepository $repo)
    {
        $tournaments = $repo->findAll();
        return $this->render('tournament/tournaments.html.twig', [
            'tournaments' => $tournaments
        ]);
    }

    /**
     * @Route("/tournament/create", name="tournament_create")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createTournament(SuperCategoryRepository $repo){
        $this->denyAccessUnlessGranted('ROLE_USER');


    }

    /**
     * @Route("/tournament/create", name="tournament_create")
     * @Route("/tournament/create/{code}", name="tournament_create_code")
     * @Method("GET")
     * @param Request $request
     * @param null $code
     * @param SuperCategoryRepository $repo
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createTournamentCode(Request $request, $code = null, SuperCategoryRepository $repo){
        $this->denyAccessUnlessGranted('ROLE_USER');
        if($code === null){
            $superCategories = $repo->findAll();
            return $this->render('tournament/choose_super_category.html.twig', [
                'superCategories' => $superCategories
            ]);
        }else{
            $tournament = new Tournament();

            $flow = $this->tournamentFlow;
            $flow->setGenericFormOptions(['code' => $code]);
            $flow->bind($tournament);

            // form of the current step
            $form = $flow->createForm();
            if ($flow->isValid($form)) {
                $flow->saveCurrentStepData($form);

                if ($flow->nextStep()) {
                    // form for the next step
                    $form = $flow->createForm();
                } else {
                    // flow finished
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($tournament);
                    $em->flush();

                    $flow->reset(); // remove step data from the session

                    return $this->redirect($this->generateUrl('home')); // redirect when done
                }
            }

            return $this->render('tournament/create.html.twig', [
                'form' => $form->createView(),
                'flow' => $flow,
            ]);
        }
    }

    /**
     * @Route("tournament/{id}/edit", name="tournament_edit")
     * @param Tournament $tournament
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editTournament(Tournament $tournament, Request $request){
        if($this->getUser() === $tournament->getUser()){
            $form = $this->createForm(TournamentType::class, $tournament);
            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){
                $this->em->flush();
                $this->addFlash('success', 'Tournoi modifié avec succès');
                return $this->redirectToRoute('tournament', array("id" => $tournament->getId()));
            }
            return $this->render('tournament/edit.html.twig', [
                'tournament' => $tournament,
                'form' => $form->createView()
            ]);
        }else{
            $this->addFlash('error', 'Vous n\'êtes pas le créateur de ce tournoi');
            return $this->redirectToRoute('tournaments');
        }

    }

    /**
     * @Route("tournament/{id}/delete", name="tournament_delete", methods="DELETE")
     * @param Tournament $tournament
     * @param Request $request
     */
    public function deleteTournament(Tournament $tournament, Request $request){
        if($this->getUser() === $tournament->getUser()) {
            if ($this->isCsrfTokenValid('delete' . $tournament->getId(), $request->get('_token'))) {
                $this->em->remove($tournament);
                $this->em->flush();
                $this->addFlash('success', 'Tournoi supprimé avec succès');
            }
            return $this->redirectToRoute('tournaments');
        }else{
            $this->addFlash('error', 'Vous n\'êtes pas le créateur de ce tournoi');
            return $this->redirectToRoute('tournaments');
        }
    }

    /**
     * @Route("/tournament/{id}", name="tournament")
     * @param Tournament $tournament
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function tournament(Tournament $tournament)
    {
        return $this->render('tournament/tournament.html.twig', [
            'tournament' => $tournament
        ]);
    }



}
