<?php

namespace App\Controller;

use App\Entity\UserFilter;
use App\Form\UserFilterType;
use Doctrine\Common\Persistence\ObjectManager;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Repository\UserRepository;

class AdminController extends AbstractController
{

    protected $em;
    protected $userRepository;

    public function __construct(UserRepository $userRepository, ObjectManager $em)
    {
        $this->em = $em;
        $this->userRepository = $userRepository;
    }

    /**
     * @Route("/admin", name="admin")
     */
    public function index(PaginatorInterface $paginator, Request $request)
    {
        $users = $paginator->paginate(
            $this->userRepository->findAllActiveQuery(),
            $request->query->getInt('page', 1),
            1
            );
        return $this->render('admin/index.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @Route("/admin/users", name="admin_users")
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function users(PaginatorInterface $paginator, Request $request){
        $filter = new UserFilter();
        $form = $this->createForm(UserFilterType::class, $filter);
        $form->handleRequest($request);

        $users = $paginator->paginate(
            $this->userRepository->findAllQuery($filter),
            $request->query->getInt('page', 1),
            5
        );
        return $this->render('admin/users.html.twig', [
            'users' => $users,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/user/{id}/edit", name="admin_user_edit")
     * @param User $user
     */
    public function editUser(User $user){

    }

    /**
     * @Route("/admin/categories", name="admin_categories")
     */
    public function categories(){

    }
}
