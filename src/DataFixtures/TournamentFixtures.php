<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\SuperCategory;
use App\Entity\Tournament;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @property  encoder
 */
class TournamentFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername('admin')
            ->setPassword($this->encoder->encodePassword($user, 'admin'))
            ->setEmail("admin@email.fr")
            ->setIsActive(true)
            ->setCreatedAt(new \DateTime())
            ->setRoles(["ROLE_SUPER_ADMIN", "ROLE_ADMIN", "ROLE_USER"]);

        $manager->persist($user);

        $numTournament = 0;
        $superCategories = array(
            array("Sport", "sport", "https://safebrands.fr/wp-content/uploads/2018/08/sports.jpg"),
            array("Jeux vidéo", "videogames", "http://www.assoc-lemail.net/wp-content/uploads/2018/10/gamer.jpg")
        );
        for($n=0; $n<sizeof($superCategories); $n++){
            $superCategory = new SuperCategory();
            $superCategory->setTitle($superCategories[$n][0]);
            $superCategory->setCode($superCategories[$n][1]);
            $superCategory->setImage($superCategories[$n][2]);
            $manager->persist($superCategory);

            if($superCategories[$n][1] === "sport"){
                $categories = array(
                    array("Foot 5","http://f5-footfive.fr/wp-content/uploads/2015/11/foot-five-011.jpg"),
                    array("Tennis","https://media1.woopic.com/api/v1/images/504%2Fmagic-article-sports%2Fd0a%2Fd77%2F07f8c51109bd03e4fd6c3e5ef8%2Fdes-joueurs-de-tennis-harceles-et-menaces-par-des-parieurs%7Cd0ad7707f8c51109bd03e4fd6c3e5ef8.jpg?format=980x450&facedetect=1&quality=85")
                );
                for($i=0; $i<sizeof($categories); $i++){
                    $category = new Category();
                    for($j=0; $j<sizeof($categories[$i]); $j++){
                        if($j == 0){
                            $category->setTitle($categories[$i][$j]);
                        }else{
                            $category->setImage($categories[$i][$j]);
                            $category->setSuperCategory($superCategory);
                            $manager->persist($category);
                        }
                        for($k=0; $k<2; $k++){
                            $tournament = new Tournament();
                            $numTournament++;
                            $tournament
                                ->setTitle("Tournoi n°".$numTournament)
                                ->setDescription("Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium ad assumenda atque corporis culpa debitis ea error fugit harum, ipsum, itaque iusto laudantium minus omnis quidem, rem unde voluptate! Ad deleniti magni nesciunt officia quisquam veritatis? Accusantium consectetur deleniti, esse exercitationem inventore non nostrum odit placeat, quaerat ratione rerum, sint!")
                                ->setSize(10)
                                ->setTeam(true)
                                ->setTeamMaxSize(5)
                                ->setTeamMinSize(2)
                                ->setPublic(true)
                                ->setCreatedAt(new \DateTime())
                                ->setState(true)
                                ->setUser($user)
                                ->setCategory($category);

                            $manager->persist($tournament);
                        }
                    }
                }
            }

            if($superCategories[$n][1] === "videogames"){
                $categories = array(
                    array("League of Legends","https://static.lpnt.fr/images/2013/10/25/lol-league-of-legends-2070239-jpg_1824466_660x281.JPG"),
                    array("Counter Strike : Global Offensive","http://lesplayersdudimanche.com/wp-content/uploads/2016/09/Counter-Strike-Global-Offensive-League-810x400.jpg")
                );
                for($i=0; $i<sizeof($categories); $i++){
                    $category = new Category();
                    for($j=0; $j<sizeof($categories[$i]); $j++){
                        if($j == 0){
                            $category->setTitle($categories[$i][$j]);
                        }else{
                            $category->setImage($categories[$i][$j]);
                            $category->setSuperCategory($superCategory);
                            $manager->persist($category);
                        }
                        for($k=0; $k<2; $k++){
                            $tournament = new Tournament();
                            $numTournament++;
                            $tournament
                                ->setTitle("Tournoi n°".$numTournament)
                                ->setDescription("xLorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium ad assumenda atque corporis culpa debitis ea error fugit harum, ipsum, itaque iusto laudantium minus omnis quidem, rem unde voluptate! Ad deleniti magni nesciunt officia quisquam veritatis? Accusantium consectetur deleniti, esse exercitationem inventore non nostrum odit placeat, quaerat ratione rerum, sint!")
                                ->setSize(10)
                                ->setTeam(true)
                                ->setTeamMaxSize(5)
                                ->setTeamMinSize(2)
                                ->setPublic(true)
                                ->setCreatedAt(new \DateTime())
                                ->setState(true)
                                ->setUser($user)
                                ->setCategory($category);

                            $manager->persist($tournament);
                        }
                    }
                }
            }
        }
        $manager->flush();
    }
}
