<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for($i=0; $i<15; $i++){
            $user = new User();
            $user
                ->setUsername($faker->userName)
                ->setPassword($faker->password)
                ->setEmail($faker->email)
                ->setIsActive(true)
                ->setCreatedAt(new \DateTime())
                ->setRoles(["ROLE_USER"]);
            $manager->persist($user);
        }
        $manager->flush();
    }
}
